﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clase_MVC.Models;

namespace Clase_MVC.Controllers
{
    public class PeliculaController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();

        //
        // GET: /Pelicula/

        public ActionResult Index()
        {
            return View(db.peliculas.ToList());
        }

        //
        // GET: /Pelicula/Details/5

        public ActionResult Details(int id = 0)
        {
            PeliculaModels peliculamodels = db.peliculas.Find(id);
            if (peliculamodels == null)
            {
                return HttpNotFound();
            }
            return View(peliculamodels);
        }

        //
        // GET: /Pelicula/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Pelicula/Create

        [HttpPost]
        public ActionResult Create(PeliculaModels peliculamodels)
        {
            if (ModelState.IsValid)
            {
                db.peliculas.Add(peliculamodels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(peliculamodels);
        }

        //
        // GET: /Pelicula/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PeliculaModels peliculamodels = db.peliculas.Find(id);
            if (peliculamodels == null)
            {
                return HttpNotFound();
            }
            return View(peliculamodels);
        }

        //
        // POST: /Pelicula/Edit/5

        [HttpPost]
        public ActionResult Edit(PeliculaModels peliculamodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(peliculamodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliculamodels);
        }

        //
        // GET: /Pelicula/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PeliculaModels peliculamodels = db.peliculas.Find(id);
            if (peliculamodels == null)
            {
                return HttpNotFound();
            }
            return View(peliculamodels);
        }

        //
        // POST: /Pelicula/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliculaModels peliculamodels = db.peliculas.Find(id);
            db.peliculas.Remove(peliculamodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}