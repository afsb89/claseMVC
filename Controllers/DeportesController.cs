﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clase_MVC.Controllers
{
    public class DeportesController : Controller
    {
        //
        // GET: /Deportes/

        public ActionResult Index()//string categoria = "FUTBOL")
        {
            //ViewBag.deporte = categoria;
            /*Devuelve un recurso*/
            //return RedirectToAction("Index", "Home");

            /*Devuelve una pagina*/
            //return RedirectToRoute("Default", new { Controller = "Home", Action = "About" });

            /*Devuelve un json*/
            //var mensaje = Server.HtmlEncode(categoria);
            //return Json(new { Message = mensaje, Name = "deporte" }, JsonRequestBehavior.AllowGet);

            /*Devuelve un archivo*/
            //return File(Server.MapPath("~/Content/site.css"),"text/css");

            /*Devuelve texto*/
            return View();
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string valor) {
            ViewBag.deporte = valor;
            return View();
        }
    }
}
