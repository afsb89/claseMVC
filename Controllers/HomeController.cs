﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clase_MVC.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Inicio.";
            ViewBag.Variable = "Hola";

            return View();
        }

        //[ActionName("Acerca")]
        public ActionResult About()
        {
            ViewBag.Message = "Información.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contacto.";

            return View();
        }

        [ActionName("Acerca")]
        public ActionResult ActionNameTest()
        {
            ViewBag.Message = "Información.";

            return View();
        }
    }
}
