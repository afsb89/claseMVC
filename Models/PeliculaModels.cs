﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Clase_MVC.Models
{
    public class PeliculaModels
    {
        public int id { get; set; }
        [Required(ErrorMessage = "** {0} es requerido **")]
        [Display(Name = "Titulo")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "** El {0} requiere al menos {2} caracteres **")]
        public string titulo { get; set; }

        [Display(Name = "Actor Principal")]
        public string actorPrincipal { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        [Display(Name = "Publicación")]
        public DateTime Fecha { get; set; }
    }

    public class PeliculaDBContext : DbContext {
        public PeliculaDBContext() : base("PeliculasConnection") { }
        public DbSet<PeliculaModels> peliculas { get; set; }
    }
}